require 'rest-client'

module Line
    module MessageApi
        ACCESS_TOKEN = Rails.configuration.line["message_api"]["channel_access_token"]
        SEND_REPLY_MESSAGE_URL = "https://api.line.me/v2/bot/message/reply"
	    DEFAULT_MESSAGE=[{
            "type": "text",
            "text": "Create TODO Format - `<TASK> : <today|tomorrow|DD/MM/YY> : <HH:MM>`"
        }]
        
        module_function
        def send_reply_message(reply_token, messages)
            begin
                RestClient.post(
                    "https://api.line.me/v2/bot/message/reply",
                    {
                        "replyToken": reply_token,
                        "messages": messages
                    }.to_json,
                    headers={
                        "Content-Type" => "application/json",
                        "Authorization" => "Bearer #{ACCESS_TOKEN}"
                    }
                )
            rescue RestClient::UnsupportedMediaType => e
                puts e.response.inspect
                puts e.response.body
            rescue RestClient::BadRequest => e
                puts e.response.inspect
                puts e.response.body
            end
        end

        def send_push_message(users, messages)
            begin
                RestClient.post(
                    "https://api.line.me/v2/bot/message/multicast",
                    {
                        "to": users,
                        "messages": messages
                    }.to_json,
                    headers={
                        "Content-Type" => "application/json",
                        "Authorization" => "Bearer #{ACCESS_TOKEN}"
                    }
                )
            rescue RestClient::UnsupportedMediaType => e
                puts e.response.inspect
                puts e.response.body
            end
        end
    end
end
