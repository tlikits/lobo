class CreateTodos < ActiveRecord::Migration[5.2]
  def change
    create_table :todos do |t|
      t.string :task
      t.datetime :due_date
      t.references :user, foreign_key: true
      t.boolean :is_done, :default => false
      t.boolean :is_pinned, :default => false

      t.timestamps
    end
  end
end

#User.first.todos.create
#(:task => "test", :due_date => "2018-05-30 10:18:11", :is_done => true)