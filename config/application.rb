require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Lobo
  class Application < Rails::Application
    config.load_defaults 5.2
    config.line = config_for(:line)
    config.autoload_paths += %W(#{config.root}/lib)
  end
end
