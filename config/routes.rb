Rails.application.routes.draw do
  get '/login', :to => 'sessions#login'
  devise_for :users, :path => "accounts", :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  match '/webhook/line' => 'webhook/line#webhook', :via => :post

  root 'todos#index'
  resources :todos, :only => [:index, :create]
  scope :api do
    scope :todos do 
      get '/test', :to => 'api/todos#create'
      post '/:id/task', :to => 'api/todos#update_task'
      post '/:id/due_date', :to => 'api/todos#update_due_date'
      post '/:id/pinned', :to => 'api/todos#update_is_pinned'
      post '/:id/done', :to => 'api/todos#update_is_done'
    end
  end
end
