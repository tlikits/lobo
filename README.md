# Lobo - Line Bot Todo!
## **How to install on Heroku**

### **Requirement**
* Line Messaging API
* Line Login
* Postgres Node


Clone this git
```
$ git clone https://bitbucket.org/tlikits/lobo.git
```

To deploy on heroku,
commit your configuration first, then
```
$ heroku create
$ git push heroku master
```

To setup database, set environment variables on Heroku. (This require Postgres)
```
$ heroku config:set \
    POSTGRES_HOST=<POSTGRES_HOST> \
    POSTGRES_PORT=<POSTGRES_HOST> \
    POSTGRES_DATABASE=<POSTGRES_DATABASE> \
    POSTGRES_USERNAME=<POSTGRES_USERNAME> \
    POSTGRES_PASSWORD=<POSTGRES_PASSWORD>
```

To setup Line Config, for both Message API and Line Login, set environment variables on Heroku.
```
$ heroku config:set \
    MESSAGE_API_CHANNEL_ID=<MESSAGE_API_CHANNEL_ID> \
    MESSAGE_API_CHANNEL_SECRET=<MESSAGE_API_CHANNEL_SECRET> \
    MESSAGE_API_ACCESS_TOKEN=<MESSAGE_API_ACCESS_TOKEN> \
    LINE_LOGIN_CHANNEL_ID=<LINE_LOGIN_CHANNEL_ID> \
    LINE_LOGIN_CHANNEL_SECRET=<LINE_LOGIN_CHANNEL_SECRET>
```

Generate secret key.
```
$ rails secret
```

Copy the result, and set to Heroku environment,
```
heroku config:set SECRET_KEY_BASE=<YOUR_RESULT>
```

Clone this git.
```
$ git clone https://tlikits@bitbucket.org/tlikits/lobo.git
```
Push project to heroku.
```
$ git push heroku master
```

Migrate database on Heroku
```
heroku run rails db:migrate
```

**Open app** on Heroku!