require "line/message_api"

class Webhook::LineController < ApplicationController
    skip_before_action :verify_authenticity_token

    def webhook
        if !signature_valid?(request)
            render json: { message: "invalid signature" }
            return
        end

        @events = params["events"]
        @events.each do |event|
            @user_id = [event["source"]["userId"]]
            @user_message = event["message"]["text"]
            @reply_token = event["replyToken"]

            @user = User.find_by_uid(@user_id)
            if @user.nil?
                Line::MessageApi.send_reply_message(@reply_token, get_signup_message)
                render json: { message: "done"}
                return
            end

            if @user_message.downcase == 'edit'
                send_edit_page
            elsif @user_message.include? " : "
                create_todo
            else
                Line::MessageApi.send_reply_message(@reply_token, Line::MessageApi::DEFAULT_MESSAGE)
            end
        end

        render json: { message: "done"}
    end


    private
    def send_edit_page
        messages = [{
            "type": "text",
            "text": "You can edit your todo list at #{todos_url}"
        }]
        Line::MessageApi.send_reply_message(@reply_token, messages)
    end

    def create_todo
        task_arr = @user_message.split(" : ").map{ |x| x.strip() }
        task = task_arr[0]
        date = get_date(task_arr[1])
        time = task_arr[2] || "12:00:00"
        if !(due_date = get_date_time(date, time))
            Line::MessageApi.send_reply_message(@reply_token, Line::MessageApi::DEFAULT_MESSAGE)
            return
        end
        @user.todos.create({:task => task, :due_date => due_date})

        messages = [{
            "type": "text",
            "text": "Created todo - #{task} on #{due_date.strftime('%B %d, %Y, at %I:%M%p')}"
        }]
        
        Line::MessageApi.send_reply_message(@reply_token, messages)
    end


    def get_signup_message
        [{
            "type": "text",
            "text": "To use Lobo please signup/login at #{user_line_omniauth_authorize_url}"
        }]
    end

    def signature_valid?(request)
        x_line_signature = request.headers["X-Line-Signature"]
        body = request.body.read

        body_signature_hash = Base64.strict_encode64(OpenSSL::HMAC.digest(OpenSSL::Digest::SHA256.new, Rails.configuration.line["message_api"]['channel_secret'], body)).strip()
        x_line_signature == body_signature_hash
    end

    def get_date_time(date_s, time_s)
        date = get_date date_s
        datetime = "#{date_s} #{time_s}"
        begin
            datetime = DateTime.strptime(datetime, "%d/%m/%y %H:%M")
        rescue ArgumentError => e
            puts "#{e.message} - time_s: #{time_s}"
            return nil
        end
    end

    def get_date(date_s)
        case date_s
        when "today"
            return DateTime.now.to_date.strftime("%d/%m/%y")
        when "tomorrow"
           return (DateTime.now + 1.days).to_date.strftime("%d/%m/%y")
        end
        begin
            # check valid format
            DateTime.strptime(date_s, "%d/%m/%y")
            date_s
        rescue ArgumentError => e
            puts "#{e.message} - date_s: #{date_s}"
            return nil
        end
    end
end


