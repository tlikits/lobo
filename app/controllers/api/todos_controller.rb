class Api::TodosController < ApplicationController
    before_action :authenticate_line_user
    skip_before_action :verify_authenticity_token

    def create
        render :json => {'hi': 'hello'}
    end

    def update_task
        update_by_param(update_task_param)
    end

    def update_due_date
        update_by_param(update_due_date_param)
    end

    def update_is_pinned
        update_by_param(update_is_pinned_param)
    end

    def update_is_done
        update_by_param(update_is_done_param)
    end

    
    private
    def update_by_param(param)
        @todo = current_user.todos.find_by_id(params.require(:todo)[:id])
        if @todo.nil?
            render :json => { message: "not found" }, :status => :bad_request
            return
        end
        if @todo.update(param)
            render :json => { message: "success" }
        else
            render :json => { message: "failed" }, :status => :bad_request
        end
    end

    def update_task_param
        params.require(:todo).permit(:task)
    end

    def update_due_date_param
        # params['todo']['due_date'] = DateTime.strptime(params['todo']['due_date'], '%d-%m-%y %I:%M%p')
        
        formatted_param = params.require(:todo).permit(:due_date)
        puts formatted_param
        formatted_param["due_date"] = DateTime.strptime(formatted_param["due_date"], '%d/%m/%y %I:%M%p')
        puts formatted_param
        formatted_param
    end

    def update_is_pinned_param
        params.require(:todo).permit(:is_pinned)
    end

    def update_is_done_param
        params.require(:todo).permit(:is_done)
    end
end
