class ApplicationController < ActionController::Base
    def authenticate_line_user
        if current_user.nil?
			flash[:warning] = "You are not logged in."
			redirect_to login_path
		end
    end

end
