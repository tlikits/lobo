require 'rest-client'
require 'jwt'

class SessionsController < ApplicationController
    def login
        if !current_user.nil?
            redirect_to root_path
        end
    end
end
