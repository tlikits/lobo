class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def line
      @user = find_for_oauth(request.env["omniauth.auth"], current_user)
      if @user
        flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "line"
        session["devise.line_data"] = request.env["omniauth.auth"]
        sign_in_and_redirect @user, :event => :authentication
      end    
  end
  
  def new_session_path(scope)
    user_line_omniauth_authorize_path
  end

private
  def find_for_oauth(access_token, resource=nil)
      uid = access_token.uid
      refresh_token = access_token.credentials.refresh_token        
      image = access_token.info.image
      name = access_token.info.name

      if user = User.find_by_uid(uid)
        user
      else
        user = User.new(
          :uid => uid, 
          :picture => image, 
          :name => name
          )
        user.save!
      end
      return user
  end
end
