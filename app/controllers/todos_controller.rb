class TodosController < ApplicationController
    before_action :authenticate_line_user

    def index
        @todos = current_user.todos.where(is_done: false).order(is_pinned: :desc, due_date: :asc, id: :asc)
        @done_todos = current_user.todos.where(is_done: true).order(is_pinned: :desc, due_date: :asc, id: :asc)
        @todo = Todo.new
    end
end
