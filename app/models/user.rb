class User < ApplicationRecord
  devise :omniauthable, :rememberable, :trackable

  has_many :todos
end
